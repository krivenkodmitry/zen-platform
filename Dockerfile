FROM node:12.7.0

WORKDIR /usr/app

COPY . .

RUN yarn install

EXPOSE 4000

CMD ["yarn", "start"]
