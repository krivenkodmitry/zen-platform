const { PORT, NODE_ENV, SEEDS } = process.env

export default {
  port: Boolean(PORT) ? PORT : '4000',
  nodeEnv: NODE_ENV,
  seeds: SEEDS === 'true'
}
