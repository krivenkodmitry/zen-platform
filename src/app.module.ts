import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common'
import { ConfigModule, ConfigService } from 'nestjs-config'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { GraphQLModule } from '@nestjs/graphql'
import { TypeOrmModule } from '@nestjs/typeorm'
import { resolve } from 'path'
import { UserModule } from './modules/user/user.module'
import { AuthModule } from './modules/auth/auth.module'
import { PostModule } from './modules/post/post.module'

@Module({
  imports: [
    ConfigModule.load(resolve(__dirname, '**/*/!(*.d).config.{ts,js}'), {
      modifyConfigName: (name: string) => name.replace('.config', '')
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (config: ConfigService) => {

        return config.get('database')

      },
      inject: [ConfigService]
    }),
    GraphQLModule.forRootAsync({
      useFactory: (config: ConfigService) => config.get('graphql'),
      inject: [ConfigService]
    }),
    UserModule,
    AuthModule,
    PostModule
  ],
  controllers: [AppController],
  providers: [AppService]
})

export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply().forRoutes('/graphql')
  }
}
