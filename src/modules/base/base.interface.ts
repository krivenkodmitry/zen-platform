export interface IErrorOptions {
  message: string
  errorData?: any
}

export interface IResponse<T> {
  ok: boolean
  message?: string
  data: T
}
