import { CreateDateColumn, PrimaryGeneratedColumn } from 'typeorm'

export abstract class BaseEntity {

  @PrimaryGeneratedColumn('uuid')
  public id: string

  @CreateDateColumn({ name: 'created_at', type: 'timestamp with time zone' })
  public createdAt: Date


}
