import { PostService } from '../../post.service'
import { Resolver, Args, Context } from '@nestjs/graphql'
import { PostCreateDto } from '../../dtos/post-create.dto'
import { Post } from '../../post.entity'
import { IResponse } from '../../../base'
import { User } from '../../../user/user.entity'
import { Logger } from '@nestjs/common'

@Resolver('PostMutations')
export class CreatePostResolver {
  constructor(private readonly createPostService: PostService) {}

  async postCreate(
    @Args('input')input: PostCreateDto,
    @Context('currentUser') currentUser: User
  ): Promise<IResponse<Post>> {
    const { id: userId } = currentUser

    try {
      return {
        ok: true,
        data: await this.createPostService.create({userId, ...input})
      }
    } catch (e) {
      Logger.error('error while post create')
      throw new Error(e.toString())
    }
  }

}
