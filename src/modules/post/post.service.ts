import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { PostRepository } from './post.repository'
import { Post } from './post.entity'
import { EntityManager } from 'typeorm'

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(PostRepository)
    private readonly postRepository: PostRepository

  ) {}

  async create(
    attributes: Partial<Post>,
    repository: EntityManager | PostRepository = this.postRepository
  ): Promise<Post> {
    const validJobProfile = await this.postRepository.create(attributes)

    return repository.save(validJobProfile)
  }

  async getMany(filter: { condition: string; param: any }[]): Promise<Post[]> {
    return this.postRepository
      .getQueryBuilder()
      .getMany()
  }
}
