import { POST_ALIAS, Post } from 'src/modules/post/post.entity'
import { EntityRepository, Repository, SelectQueryBuilder } from 'typeorm'

@EntityRepository(Post)
export class PostRepository extends Repository<Post> {
  getQueryBuilder(): SelectQueryBuilder<Post> {
    return this.createQueryBuilder(POST_ALIAS)
  }
}
