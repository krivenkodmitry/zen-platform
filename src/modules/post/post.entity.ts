import { Column, Entity, ManyToOne, JoinColumn } from 'typeorm'
import { BaseEntity} from '../base/base.entity'
import { User } from '../user/user.entity'

export const POST_ALIAS = 'posts'

@Entity('posts')
export class Post extends BaseEntity {

  /** Columns */

  @Column({ name: 'user_id', type: 'uuid', nullable: false })
  public userId: string

  @Column( {type: 'varchar', name: 'post_title', nullable: false })
  public title: string

  @Column({ type: 'varchar', name: 'post_body', nullable: false })
  public body: string


  /** Relationships */

  @ManyToOne(() => User, (user: User) => user.posts)
  @JoinColumn({name: 'user_id', referencedColumnName: 'id'})
  public user: User

}
