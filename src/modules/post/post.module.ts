import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { CreatePostResolver } from './resolvers/mutations/create-post.resolver'
import { Post } from './post.entity'
import { PostRepository } from './post.repository'
import { PostService } from './post.service'

@Module({
  imports: [TypeOrmModule.forFeature([Post, PostRepository])],
  providers: [CreatePostResolver, PostService]
})

export class PostModule {}
