import { UserInputError } from 'apollo-server'
import { IErrorOptions } from 'src/modules/base'

export const returnDataOrErrorUtil = <T>(
  data: T,
  error: IErrorOptions = { message: 'Data not found', errorData: {} }
): T => {
  if (Boolean(data)) {
    return data
  } else {
    throw new UserInputError(error.message, error.errorData)
  }
}
