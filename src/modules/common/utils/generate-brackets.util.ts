import { Logger } from '@nestjs/common'
import { forIn, set } from 'lodash'
import { Brackets, WhereExpression } from 'typeorm'

export const generateBracketsUtil = <T, K>(
  filter: T,
  condition: K,
  customs?: { whereFn?: string; condition: string; param: any }[]
): Brackets => {
  return new Brackets((qb: WhereExpression) => {
    forIn(filter, (value: any, key: string) => {
      if (Boolean(condition[key])) {
        const param = {}
        set(param, key, value)
        qb.andWhere(condition[key], param)
      } else {
        Logger.warn(`Filter object don't have: ${key} condition`, 'Filter decorator')
      }
    })

    if (Boolean(customs) && Boolean(customs.length)) {
      customs.forEach((custom: { whereFn?: string; condition: string; param: any }) => {
        const { whereFn = 'andWhere' } = custom
        qb[whereFn](custom.condition, custom.param)
      })
    }
  })
}
