import { Module } from '@nestjs/common'
import { AuthorResolver } from './author.resolver'
import { UserModule } from '../user/user.module'
import { PostModule } from '../post/post.module'

@Module({
  providers: [AuthorResolver],
  imports: [UserModule, PostModule]
})
export class AuthorModule {}
