import { AuthSignUpInput } from 'src/graphql.schema'

export class SignUpDto implements AuthSignUpInput {
  email: string

  password: string

  firstName: string

  lastName: string
}
