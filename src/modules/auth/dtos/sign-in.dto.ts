import { AuthSignInInput } from 'src/graphql.schema'

export class SignInDto implements AuthSignInInput {

  email: string

  password: string

}
