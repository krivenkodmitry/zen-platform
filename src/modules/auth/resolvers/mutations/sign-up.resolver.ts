import { Resolver, ResolveProperty, Args } from '@nestjs/graphql'
import { UserService } from '../../../user/services/user.service'
import { SignUpDto } from '../../dtos/sign-up.dto'
import { AuthSignUpUserResponse } from 'src/graphql.schema'

@Resolver('AuthMutations')
export class SignUpResolver {
  constructor(private readonly userService: UserService) {}

  @ResolveProperty('signUp')
  async authSignUp(@Args('input') input: SignUpDto): Promise<AuthSignUpUserResponse> {
    const { password, email, lastName, firstName } = input

    try {
      const user = await this.userService.create({
        email,
        lastName,
        firstName,
        password
      })

      return {
        ok: true,
        user
      }
    } catch (error) {
      return error
    }
  }
}
