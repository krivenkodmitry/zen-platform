import { Resolver, ResolveProperty, Args } from '@nestjs/graphql'
import { UserService } from 'src/modules/user/services/user.service'
import { AuthenticationError } from 'apollo-server'
import { SignInDto } from 'src/modules/auth/dtos'
import { AuthSignUpUserResponse } from '../../../../graphql.schema'

@Resolver('AuthMutations')
export class SignInResolver {
  private readonly authErrorMessage: string = 'Email or password is not correct'

  constructor(private readonly userService: UserService) {}

  @ResolveProperty('signIn')
  async authSignIn(@Args('input') params: SignInDto): Promise<AuthSignUpUserResponse> {
    const { email, password } = params

    const user = await this.userService.getOneOrFailWithAddSelect({ email }, ['password'], {
      message: this.authErrorMessage
    })

    if (!Boolean(user.password)) {
      throw new AuthenticationError(
        `We know you signed up with Google or Facebook, therefore, you set no password.
         If you want to set a password please use reset password.`
      )
    }

    if (!Boolean(user.comparePasswords(password))) {
      throw new AuthenticationError(this.authErrorMessage)
    }

    return {
      ok: true,
      user
    }

  }
}
