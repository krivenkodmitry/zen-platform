import { Module } from '@nestjs/common'
import { SignUpResolver } from './resolvers/mutations/sign-up.resolver'
import { SignInResolver } from './resolvers/mutations/sign-in.resolver'
import { UserModule } from '../user/user.module'


@Module({
  imports: [
    UserModule
  ],
  providers: [SignUpResolver, SignInResolver]
})
export class AuthModule {}
