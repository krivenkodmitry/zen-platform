import { User, USER_ALIAS } from './user.entity'
import { EntityRepository, Repository, SelectQueryBuilder } from 'typeorm'

@EntityRepository(User)
export class UserRepository extends  Repository<User> {
  getQueryBuilder(): SelectQueryBuilder<User> {
    return this.createQueryBuilder(USER_ALIAS)
  }
}
