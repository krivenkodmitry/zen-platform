import { Module } from '@nestjs/common'
import { UserService } from './services/user.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { User } from './user.entity'
import { UserRepository } from './user.repository'

@Module({
  imports: [
    TypeOrmModule.forFeature([User, UserRepository])
  ],
  providers: [UserService],
  exports: [UserService]
})
export class UserModule {}
