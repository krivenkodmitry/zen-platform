import { Injectable } from '@nestjs/common'
import { User, USER_ALIAS } from '../user.entity'
import { UsersFilter } from 'src/modules/user/conditions'
import { generateBracketsUtil, returnDataOrErrorUtil } from 'src/modules/common/utils'
import { InjectRepository } from '@nestjs/typeorm'
import { UserRepository } from '../user.repository'
import { EntityManager } from 'typeorm'
import { IErrorOptions } from 'src/modules/base'


@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository
  ) {}

  async create(
    attributes: Partial<User>,
    repository: EntityManager | UserRepository = this.userRepository
  ): Promise<User> {
    const user = await this.userRepository.create(attributes)

    return repository.save(user)
  }

  async getOneOrFailWithAddSelect(
    condition: Partial<User>,
    select?: (keyof User)[],
    error?: IErrorOptions
  ): Promise<User> {

    const brackets = generateBracketsUtil(condition, UsersFilter)

    const query = this.userRepository.getQueryBuilder().where(brackets)

    if (Boolean(select)) {
      query.addSelect(select.map((key: string) => `${USER_ALIAS}.${key}`))
    }

    const user = await query.getOne()

    return returnDataOrErrorUtil<User>(user, error)
  }
}
