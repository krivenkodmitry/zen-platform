import { Column, Entity, OneToMany } from 'typeorm'
import { BaseEntity} from '../base/base.entity'
import { Post } from '../post/post.entity'
import bcrypt from 'bcrypt'

export const USER_ALIAS = 'users'

@Entity('users')
export class User extends BaseEntity {

  /** Columns */

  @Column({ type: 'varchar', nullable: true, unique: true })
  public email: string

  @Column({ type: 'varchar', nullable: true, select: false})
  public password: string

  @Column({name: 'first_name', type: 'varchar', nullable: false})
  public firstName: string

  @Column({name: 'last_name', type: 'varchar', nullable: false})
  public lastName: string

  @Column({ name: 'birth_date', type: 'timestamp', nullable: true})
  public birthDate: Date


  /** Relationships */

  @OneToMany(() => Post, (post: Post) => post.user)
  public posts: Post


  /** Instance Methods */

  public async comparePasswords(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.password)
  }
}
