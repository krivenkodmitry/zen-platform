import { USER_ALIAS} from 'src/modules/user/user.entity'

export const UsersFilter = {
  id: `${USER_ALIAS}.id = :id`,
  email: `${USER_ALIAS}.email = :email`,
  fullName: `${USER_ALIAS}.first_name || ' ' || ${USER_ALIAS}.last_name ILIKE '%' || :fullName || '%'`,
  country: `${USER_ALIAS}.country = :country`,
  city: `${USER_ALIAS}.city = :city`,
  gender: `${USER_ALIAS}.gender = :gender`,
  emailOrFullName: `(${USER_ALIAS}.first_name || ' ' || ${USER_ALIAS}.last_name ILIKE '%' || :emailOrFullName || '%')
                 OR (${USER_ALIAS}.email ILIKE '%' || :emailOrFullName || '%')`
}
