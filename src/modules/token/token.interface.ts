export interface IGenerateTokensOptions {
  access?: boolean
  refresh?: boolean
  activation?: boolean
  resetPassword?: boolean
  setOldPassword?: boolean
  inviteToTeam?: boolean
}

export interface IGenerateTokensResult {
  accessToken?: string
  refreshToken?: string
  activationToken?: string
  resetPasswordToken?: string
  setOldPasswordToken?: string
  inviteToTeamToken?: string
  options?: {
    accessOptions?: ITokenOptions
    refreshOptions?: ITokenOptions
    activationOptions?: ITokenOptions
    resetPasswordOptions?: ITokenOptions
    setOldPasswordOptions?: ITokenOptions
    inviteToTeamOptions?: ITokenOptions
  }
}

export interface IGetAccessToken {
  accessToken: string
  accessOptions: ITokenOptions
}

export interface IGetRefreshToken {
  refreshToken: string
  refreshOptions: ITokenOptions
}

export interface IGetActivationToken {
  activationToken: string
  activationOptions: ITokenOptions
}

export interface IGetResetPasswordToken {
  resetPasswordToken: string
  resetPasswordOptions: ITokenOptions
}

export interface IGetSetOldPasswordToken {
  setOldPasswordToken: string
  setOldPasswordOptions: ITokenOptions
}

export interface IgetInviteTiTeamToken {
  inviteToTeamToken: string
  inviteToTeamOptions: ITokenOptions
}

interface ITokenOptions {
  expiresIn: string
}

export interface ITokenPayload {
  id?: string
  email?: string
  teamId?: string
  userId?: string
  inviterId?: string
  isRegistered?: boolean
  isAccessToken?: boolean
  isRefreshToken?: boolean
  isActivationToken?: boolean
  isResetPasswordToken?: boolean
  isSetOldPasswordToken?: boolean
  isInviteToTeamToken?: boolean
}

export interface IGenerateTokenParams {
  userId?: string
  email?: string
  isRegistered?: boolean
  teamId?: string
  inviterId?: string
}

export interface IGetInviteTiTeamTokenParams {
  email?: string
  userId?: string
  isRegistered?: boolean
  teamId?: string
  inviterId?: string
}
