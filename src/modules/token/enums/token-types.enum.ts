export enum TokenTypes {
  ACCESS = 'access',
  REFRESH = 'refresh',
  ACTIVATION = 'activation',
  RESET_PASSWORD = 'resetPassword',
  SET_OLD_PASSWORD = 'setOldPassword',
  INVITE_TO_TEAM = 'inviteToTeam'
}
