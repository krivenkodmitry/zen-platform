import { Injectable } from '@nestjs/common'
import { ConfigService } from 'nestjs-config'
import { decode, JsonWebTokenError, sign, TokenExpiredError, verify } from 'jsonwebtoken'
import nanoid from 'nanoid'
import { IGetAccessToken, IGetActivationToken, IGetRefreshToken, ITokenPayload, } from '../token.interface'
import { TokenTypes } from '../enums/token-types.enum'

@Injectable()
export class TokenService {

  constructor(private readonly configService: ConfigService ) {}


  private getTokenType(notVerifiedData: ITokenPayload): string {
    if (notVerifiedData.isAccessToken) {
      return TokenTypes.ACCESS
    } else if (notVerifiedData.isRefreshToken) {
      return TokenTypes.REFRESH
    } else if (notVerifiedData.isActivationToken) {
      return TokenTypes.ACTIVATION
    } else if (notVerifiedData.isResetPasswordToken) {
      return TokenTypes.RESET_PASSWORD
    } else if (notVerifiedData.isSetOldPasswordToken) {
      return TokenTypes.SET_OLD_PASSWORD
    } else if (notVerifiedData.isInviteToTeamToken) {
      return TokenTypes.INVITE_TO_TEAM
    } else {
      throw new Error('Unknown Token type')
    }
  }

  private getAccessToken(id: string): IGetAccessToken {

    const payload = {id, salt: nanoid(), isRefreshToken: true}
    const accessOptions = { expiresIn: this.configService.get('jwt.accessExpires') }
    const accessToken = sign(payload, this.configService.get('jwt.accessSecret'), accessOptions)

    return { accessToken, accessOptions }
  }

  private getRefreshToken(id: string): IGetRefreshToken {

    const payload = { id, salt: nanoid(), isRefreshToken: true }
    const refreshOptions = { expiresIn: this.configService.get('jwt.refreshExpires') }
    const refreshToken = sign(payload, this.configService.get('jwt.refreshSecret'), refreshOptions)

    return { refreshToken, refreshOptions }
  }

  private getActivationToken(id: string): IGetActivationToken {

    const payload = {id, salt: nanoid(), isRefreshToken: true}
    const activationOptions = { expiresIn: this.configService.get('jwt.activationExpires') }
    const activationToken = sign(payload, this.configService.get('jwt.activationExpires') )

    return { activationToken, activationOptions }
  }

}
