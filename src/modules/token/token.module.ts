import { Module } from '@nestjs/common';
import { TokenService } from './services/token.service';

@Module({
  providers: [TokenService]
})
export class TokenModule {}
