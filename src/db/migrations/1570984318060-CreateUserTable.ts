import {MigrationInterface, QueryRunner, Table} from "typeorm"

export class CreateUserTable1570984318060 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(
          new Table({
              name: 'users',
              foreignKeys: [
                  {
                      name: 'FK_JOB_PROFILE_USER_ID',
                      referencedTableName: 'users',
                      referencedColumnNames: ['id'],
                      columnNames: ['user_id']
                  }
              ],
              columns: [
                  {
                      name: 'id',
                      type: 'uuid',
                      isPrimary: true,
                      default: 'public.uuid_generate_v4()',
                      isNullable: false,
                      isUnique: true
                  },
                  {
                      name: 'email',
                      type: 'varchar',
                      isNullable: true,
                      isUnique: true
                  },
                  {
                      name: 'password',
                      type: 'varchar',
                      isNullable: true
                  },
                  {
                      name: 'first_name',
                      type: 'varchar',
                      isNullable: true
                  },
                  {
                      name: 'last_name',
                      type: 'varchar',
                      isNullable: true
                  },
                  {
                      name: 'birth_date',
                      type: 'date',
                      isNullable: true
                  },
                  { name: 'created_at', type: 'timestamp with time zone', default: 'now()', isNullable: false }
              ]
          })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('users')
    }

}
