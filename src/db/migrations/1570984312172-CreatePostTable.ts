import {MigrationInterface, QueryRunner, Table} from 'typeorm'

export class CreatePostTable1570984312172 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(
          new Table({
              name: 'posts',
              foreignKeys: [
                  {
                      name: 'FK_JOB_PROFILE_USER_ID',
                      referencedTableName: 'users',
                      referencedColumnNames: ['id'],
                      columnNames: ['user_id']
                  }
              ],
              columns: [
                  {
                      name: 'id',
                      type: 'uuid',
                      isPrimary: true,
                      default: 'public.uuid_generate_v4()',
                      isNullable: false,
                      isUnique: true
                  },
                  {
                      name: 'post_title',
                      type: 'varchar',
                      isNullable: true,
                      isUnique: true
                  },
                  {
                      name: 'post_body',
                      type: 'varchar',
                      isNullable: true
                  },
                  { name: 'user_id', type: 'uuid', isNullable: false },
                  { name: 'created_at', type: 'timestamp with time zone', default: 'now()', isNullable: false }
              ]
          })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('posts')
    }

}
