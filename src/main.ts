import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { Logger } from '@nestjs/common'
import { ConfigService } from 'nestjs-config'
import bodyParser from 'body-parser'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  const configService = app.get(ConfigService)
  const { port, nodeEnv } = configService.get('bootstrap')
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: true }))
  await app.listen(port)
  Logger.log(`Server running on http://localhost:${port}`, 'Bootstrap')

  return nodeEnv
}

bootstrap()
  .then((nodeEnv: string) => {
    Logger.log(nodeEnv, 'nodeEnv')
    Logger.log(`☯️🧘‍ Started at "${process.env.NODE_ENV}" mode️ 🧘🕉`, 'Bootstrap')
  })
  .catch((error: Error) => {
    Logger.error('Unhandled Rejection', error.stack, 'Bootstrap')
  })
