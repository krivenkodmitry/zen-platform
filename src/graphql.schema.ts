
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export interface AuthSignInInput {
    email: string;
    password: string;
}

export interface AuthSignUpInput {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
}

export interface PostCreateInput {
    title: string;
    body: string;
}

export interface AuthMutations {
    signUp?: AuthSignUpUserResponse;
    signIn?: AuthSignUpUserResponse;
}

export interface Author {
    id: number;
    firstName?: string;
    lastName?: string;
    posts?: Post[];
}

export interface AuthSignUpUserResponse {
    ok?: boolean;
    user?: User;
}

export interface IMutation {
    auth(): AuthMutations | Promise<AuthMutations>;
    _(): boolean | Promise<boolean>;
    post(): PostMutations | Promise<PostMutations>;
}

export interface Post {
    id?: string;
    userId?: string;
    title?: string;
    body?: string;
}

export interface PostMutations {
    create: PostResponse;
}

export interface PostResponse {
    id?: string;
    title?: string;
    body?: string;
    user?: User;
}

export interface IQuery {
    hello(name?: string): string | Promise<string>;
}

export interface User {
    id?: string;
    createdAt?: DateString;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
}

export type DateString = any;
export type JSON = any;
